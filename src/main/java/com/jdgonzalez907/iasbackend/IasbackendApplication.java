package com.jdgonzalez907.iasbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IasbackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(IasbackendApplication.class, args);
	}

}

