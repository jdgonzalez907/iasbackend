package com.jdgonzalez907.iasbackend.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.jdgonzalez907.iasbackend.entities.TontAve;

public interface TontAveRepository extends JpaRepository<TontAve, String> {
	
	/**
	 * Buscar por nombre y zona
	 * 
	 * @param name
	 * @param zone
	 * @param pageable
	 * @return Page<TontAve>
	 */
	@Query("SELECT a FROM TontAve a " + 
			"JOIN a.tontavepais ap JOIN ap.tontpais p JOIN p.tontzona z " +
			"WHERE (LOWER(a.dsnombrecomun) LIKE LOWER(CONCAT('%',:name, '%')) OR LOWER(a.dsnombrecientifico) LIKE LOWER(CONCAT('%',:name, '%'))) " +
			"AND z.cdzona = :zone"
	)
	Page<TontAve> findByNameAndZone(@Param("name") String name, @Param("zone") String zone, Pageable pageable);
	
}