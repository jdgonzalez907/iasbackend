package com.jdgonzalez907.iasbackend.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.jdgonzalez907.iasbackend.entities.TontZona;

public interface TontZonaRepository extends JpaRepository<TontZona, String> {

}
