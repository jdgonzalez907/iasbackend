package com.jdgonzalez907.iasbackend.forms;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class TontAveUpdateForm {
	
	@NotEmpty
	@Size(max=100)
	private String dsnombrecomun;
	
	@NotEmpty
	@Size(max=100)
	private String dsnombrecientifico;

	public String getDsnombrecomun() {
		return dsnombrecomun;
	}

	public void setDsnombrecomun(String dsnombrecomun) {
		this.dsnombrecomun = dsnombrecomun;
	}

	public String getDsnombrecientifico() {
		return dsnombrecientifico;
	}

	public void setDsnombrecientifico(String dsnombrecientifico) {
		this.dsnombrecientifico = dsnombrecientifico;
	}

}
