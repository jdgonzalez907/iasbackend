package com.jdgonzalez907.iasbackend.forms;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class TontAveCreateForm extends TontAveUpdateForm {

	@NotEmpty
	@Size(max=5)
	private String cdave;

	public String getCdave() {
		return cdave;
	}

	public void setCdave(String cdave) {
		this.cdave = cdave;
	}

}
