package com.jdgonzalez907.iasbackend.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jdgonzalez907.iasbackend.entities.TontZona;
import com.jdgonzalez907.iasbackend.repositories.TontZonaRepository;

@RestController
@RequestMapping(path="/api/zonas")
public class TontZonaController {

	@Autowired
	private TontZonaRepository _tontZonaRepository;
	
	/**
	 * Obtener zonas
	 * 
	 * @return List<TontZona>
	 */
	@GetMapping()
	public List<TontZona> get() {
		return _tontZonaRepository.findAll();
	}
	
}
