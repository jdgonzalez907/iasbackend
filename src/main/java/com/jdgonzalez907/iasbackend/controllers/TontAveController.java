package com.jdgonzalez907.iasbackend.controllers;

import java.util.Optional;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.jdgonzalez907.iasbackend.entities.TontAve;
import com.jdgonzalez907.iasbackend.forms.TontAveCreateForm;
import com.jdgonzalez907.iasbackend.forms.TontAveUpdateForm;
import com.jdgonzalez907.iasbackend.repositories.TontAveRepository;

/**
 * @author JUAN
 *
 */
@RestController
@RequestMapping(path="/api/aves")
public class TontAveController {
	
	@Autowired
	private TontAveRepository _tontAveRepository;
	
	
	/**
	 * Obtener aves con paginación y ordenamiento
	 * 
	 * @param pageable
	 * @return Page<TontAve>
	 */
	@GetMapping()
	public Page<TontAve> get(Pageable pageable) {
		return _tontAveRepository.findAll(pageable);
	}
	
	/**
	 * Obtener aves con paginación y ordenamiento filtrando por nombre y zona
	 * 
	 * @param name
	 * @param zone
	 * @param pageable
	 * @return Page<TontAve>
	 */
	@GetMapping(path="search")
	public Page<TontAve> search(@RequestParam(required=false, defaultValue="") String name,
			@RequestParam(required=false, defaultValue="") String zone,
			Pageable pageable) {
		
		return _tontAveRepository.findByNameAndZone(
				name, 
				zone,
				pageable);
	}
	
	/**
	 * Obtener ave
	 * 
	 * @param cdave
	 * @return TontAve
	 */
	@GetMapping(path="{id}")
	public TontAve getById(@PathVariable("id") String cdave) {
		Optional<TontAve> ave = _tontAveRepository.findById(cdave);
		if (!ave.isPresent()) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No existe");
		}
		return ave.get();
	}
	
	/**
	 * Crear ave
	 * 
	 * @param tontAveForm
	 * @return TontAve
	 */
	@PostMapping()
	public TontAve post(@Valid @RequestBody TontAveCreateForm tontAveForm) {
		Optional<TontAve> ave = _tontAveRepository.findById(tontAveForm.getCdave());
		if (ave.isPresent()) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "El ID ya existe.");
		}
		TontAve newAve = new TontAve();
		newAve.setCdave(tontAveForm.getCdave());
		newAve.setDsnombrecomun(tontAveForm.getDsnombrecomun());
		newAve.setDsnombrecientifico(tontAveForm.getDsnombrecientifico());
		return _tontAveRepository.save(newAve);
	}
	
	/**
	 * Actualizar ave
	 * 
	 * @param cdave
	 * @param tontAveForm
	 * @return TontAve
	 */
	@PutMapping(path="{id}")
	public TontAve put(@PathVariable("id") String cdave, @Valid @RequestBody TontAveUpdateForm tontAveForm) {
		Optional<TontAve> ave = _tontAveRepository.findById(cdave);
		if (!ave.isPresent()) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No existe");
		}
		TontAve updateAve = new TontAve();
		updateAve.setCdave(cdave);
		updateAve.setDsnombrecomun(tontAveForm.getDsnombrecomun());
		updateAve.setDsnombrecientifico(tontAveForm.getDsnombrecientifico());
		return _tontAveRepository.save(updateAve);
	}
	
	/**
	 * Eliminar ave
	 * 
	 * @param cdave
	 */
	@DeleteMapping(path="{id}")
	public void delete(@PathVariable("id") String cdave) {
		Optional<TontAve> ave = _tontAveRepository.findById(cdave);
		if (!ave.isPresent()) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No existe");
		}
		_tontAveRepository.delete(ave.get());
	}
}
