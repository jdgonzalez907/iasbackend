package com.jdgonzalez907.iasbackend.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="TONT_AVES")
public class TontAve implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="CDAVE", length=5)
	private String cdave;
	
	@Column(name="DSNOMBRE_COMUN", length=100, nullable=false)
	private String dsnombrecomun;
	
	@Column(name="DSNOMBRE_CIENTIFICO", length=100, nullable=false)
	private String dsnombrecientifico;
	
	@OneToMany(mappedBy="tontave", cascade=CascadeType.ALL)
	@JsonIgnore
	private List<TontAvePais> tontavepais = new ArrayList<TontAvePais>();
	
	public String getCdave() {
		return cdave;
	}

	public void setCdave(String cdave) {
		this.cdave = cdave;
	}

	public String getDsnombrecomun() {
		return dsnombrecomun;
	}

	public void setDsnombrecomun(String dsnombrecomun) {
		this.dsnombrecomun = dsnombrecomun;
	}

	public String getDsnombrecientifico() {
		return dsnombrecientifico;
	}

	public void setDsnombrecientifico(String dsnombrecientifico) {
		this.dsnombrecientifico = dsnombrecientifico;
	}

	public List<TontAvePais> getTontavepais() {
		return tontavepais;
	}

	public void setTontavepais(List<TontAvePais> tontavepais) {
		this.tontavepais = tontavepais;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public TontAve () {}

	public TontAve(String cdave, String dsnombrecomun, String dsnombrecientifico, List<TontAvePais> tontavepais) {
		super();
		this.cdave = cdave;
		this.dsnombrecomun = dsnombrecomun;
		this.dsnombrecientifico = dsnombrecientifico;
		this.tontavepais = tontavepais;
	}
}
