package com.jdgonzalez907.iasbackend.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="TONT_PAISES")
public class TontPais implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="CDPAIS", length=3)
	private String cdzona;
	
	@Column(name="DSNOMBRE", length=100, nullable=false)
	private String dsnombre;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="CDZONA")
	private TontZona tontzona;
	
	@OneToMany(mappedBy="tontpais", cascade=CascadeType.ALL)
	@JsonIgnore
	private List<TontAvePais> tontavepais = new ArrayList<TontAvePais>();

	public String getCdzona() {
		return cdzona;
	}

	public void setCdzona(String cdzona) {
		this.cdzona = cdzona;
	}

	public String getDsnombre() {
		return dsnombre;
	}

	public void setDsnombre(String dsnombre) {
		this.dsnombre = dsnombre;
	}

	public TontZona getTontzona() {
		return tontzona;
	}

	public void setTontzona(TontZona tontzona) {
		this.tontzona = tontzona;
	}

	public List<TontAvePais> getTontavepais() {
		return tontavepais;
	}

	public void setTontavepais(List<TontAvePais> tontavepais) {
		this.tontavepais = tontavepais;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	public TontPais () {}

	public TontPais(String cdzona, String dsnombre, TontZona tontzona, List<TontAvePais> tontavepais) {
		this.cdzona = cdzona;
		this.dsnombre = dsnombre;
		this.tontzona = tontzona;
		this.tontavepais = tontavepais;
	}

}
