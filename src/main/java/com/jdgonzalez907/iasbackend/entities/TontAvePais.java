package com.jdgonzalez907.iasbackend.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="TONT_AVES_PAIS")
public class TontAvePais implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@JoinColumn(name="CDPAIS")
	@ManyToOne(fetch=FetchType.LAZY)
	@JsonIgnore
	private TontPais tontpais;
	
	@Id
	@JoinColumn(name="CDAVE")
	@ManyToOne(fetch=FetchType.LAZY)
	@JsonIgnore
	private TontAve tontave;
	
	public TontPais getTontpais() {
		return tontpais;
	}

	public void setTontpais(TontPais tontpais) {
		this.tontpais = tontpais;
	}

	public TontAve getTontave() {
		return tontave;
	}

	public void setTontave(TontAve tontave) {
		this.tontave = tontave;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public TontAvePais () {}

	public TontAvePais(TontPais tontpais, TontAve tontave) {
		super();
		this.tontpais = tontpais;
		this.tontave = tontave;
	}

}
