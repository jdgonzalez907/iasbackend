package com.jdgonzalez907.iasbackend.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="TONT_ZONAS")
public class TontZona implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="CDZONA", length=3)
	private String cdzona;
	
	@Column(name="DSNOMBRE", length=45, nullable=false)
	private String dsnombre;
	
	@OneToMany(mappedBy = "tontzona", cascade=CascadeType.ALL)
	@JsonIgnore
	private List<TontPais> tontpaises = new ArrayList<TontPais>();
	
	public String getCdzona() {
		return cdzona;
	}

	public void setCdzona(String cdzona) {
		this.cdzona = cdzona;
	}

	public String getDsnombre() {
		return dsnombre;
	}

	public void setDsnombre(String dsnombre) {
		this.dsnombre = dsnombre;
	}

	public List<TontPais> getTontpaises() {
		return tontpaises;
	}

	public void setTontpaises(List<TontPais> tontpaises) {
		this.tontpaises = tontpaises;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public TontZona() {}

	public TontZona(String cdzona, String dsnombre, List<TontPais> tontpaises) {
		this.cdzona = cdzona;
		this.dsnombre = dsnombre;
		this.tontpaises = tontpaises;
	}
}
